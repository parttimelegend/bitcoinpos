ANNOUNCEMENT
============

Unfortunately my computer has died and due to a lack of continous employment over the recent months I am currently unable to purchase a replacement machine at this point.

I will therefore no longer be developing this software until I am able to purchase a replacement.

It is always a little rude to ask for donations, but as I am developing this software free of charge for everyone to use. I will therefore be accepting donations towards the purchase of a new machine.

BTC: [1CdzakhzxS5QMfe9bM6KZapNWbFtfNKimx](bitcoin:1CdzakhzxS5QMfe9bM6KZapNWbFtfNKimx) | TRC: [1HgyXPsAhyD6wCGqp5Ntgq8549XkWTauvG](terracoin:1HgyXPsAhyD6wCGqp5Ntgq8549XkWTauvG) | LTC: [LRqjHkL5wFt9JxE5vmHGcxNCbqQMvyEJY5](litecoin:LRqjHkL5wFt9JxE5vmHGcxNCbqQMvyEJY5)

Okay, I'll also take [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=G2M23XDAB8HBA)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Welcome to Bitcoin POS!
-----------------------

My name is [Antony Bailey](http://www.antonybailey.net) and I'll be your author for today.

The system is **still** under development, in fact it's only just started.

Well let's begin. I originally decided to start on a [Bitcoin](http://www.bitcoin.org) Point of Sale management to work with [Blockchain.info](http://www.blockchain.info) and then I came to realise it would be a better plan to make a completely intergrated [Bitcoin](http://www.bitcoin.org) node with the features needed.

The features of the system are:

*   Connection to Bitcoin Network
*   Full Node with local Blockchain
*   Wallet Backup
*   Wallet Encryption
*   Unique Address per Transaction
*   QR Code Generation
*   Sweep Private Keys
*   [Mt.Gox](http://www.mtgox.com) Price Ticker
*   Customer CRM
*   Products Database
*   Stock Control
*   Customer Export
*   Product Export
*   Sales Export

Third Party Libaries:

- [BitcoinSharp](http://nuget.org/packages/BitCoinSharp/)
- [Entity Framework 5.0](http://nuget.org/packages/EntityFramework/5.0.0)
- [BouncyCastle Crypto](http://nuget.org/packages/BouncyCastle/)
	
Support:
Obviously this is free software, as is the [Bitcoin](http://www.bitcoin.org) protocol. I am developing this in my own time and am not being paid to do so. I will gladly accept donations, to my Bitcoin address.

Bitcoin: [14hSGGYXa85UbjryvkdMFKwJe3McFi6Ww9](http://bitcoin:14hSGGYXa85UbjryvkdMFKwJe3McFi6Ww9)

![qrcode][qrcode]
[qrcode]:https://blockchain.info/qr?data=14hSGGYXa85UbjryvkdMFKwJe3McFi6Ww9&size=200

Thanks to everyone who sent me ![balance][balance].
[balance]:http://bitcoinbox.ru/display/graph.php?btcaddress=14hSGGYXa85UbjryvkdMFKwJe3McFi6Ww9

[![Build Status](https://travis-ci.org/PartTimeLegend/BitcoinPOS.png?branch=master)](https://travis-ci.org/PartTimeLegend/BitcoinPOS)

[![Flattr this git repo](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=parttimelegend&url=https://github.com/PartTimeLegend/BitcoinPOS&title=Bitcoin POS&language=&tags=github&category=software) 
